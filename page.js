let fileHandle;

/**
 * Listener that receives a message with a list of image
 * URL's to display from popup.
 */
chrome.runtime.onMessage
    .addListener((message, sender, sendResponse) => {
        addImagesToContainer(message)
        sendResponse("OK");
    });

/**
 * Function that used to display an UI to display a list
 * of images
 * @param {} urls - Array of image URLs
 */
function addImagesToContainer(urls) {
    if (!urls || !urls.length) {
        return;
    }
    const container = document.querySelector(".container");
    urls.forEach(url => addImageNode(container, url))
}

/**
 * Function dynamically add a DIV with image and checkbox to
 * select it to the container DIV
 * @param {*} container - DOM node of a container div
 * @param {*} url - URL of image
 */
function addImageNode(container, url) {
    const div = document.createElement("div");
    div.className = "imageDiv";
    const img = document.createElement("img");
    img.src = url;
    div.appendChild(img);
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.setAttribute("url", url);
    div.appendChild(checkbox);
    container.appendChild(div)
}

/**
 * The "Select All" checkbox "onChange" event listener
 * Used to check/uncheck all image checkboxes
*/
document.getElementById("selectAll")
    .addEventListener("change", (event) => {
        const items = document.querySelectorAll(".container input");
        for (let item of items) {
            item.checked = event.target.checked;
        };
    });

/**
 * Function used to get URLs of all selected image
 * checkboxes
 * @returns Array of URL string
 */
function getSelectedUrls() {
    const urls =
        Array.from(document.querySelectorAll(".container input"))
            .filter(item => item.checked)
            .map(item => item.getAttribute("url"));
    if (!urls || !urls.length) {
        throw new Error("Please, select at least one image");
    }
    return urls;
}

/**
 * Function used to download all image files, identified
 * by `urls`, and compress them to a ZIP
 * @param {} urls - list of URLs of files to download
 * @returns a BLOB of generated ZIP-archive
 */

/**
 * Function used to return a file name for
 * image blob only if it has a correct image type
 * and positive size. Otherwise throws an exception.
 * @param {} index - An index of URL in an input
 * @param {*} blob - BLOB with a file content
 * @returns
 */
function checkAndGetFileName(index, blob) {
    let name = parseInt(index) + 1;
    const [type, extension] = blob.type.split("/");
    if (type != "image" || blob.size <= 0) {
        throw Error("Incorrect content");
    }
    return name + "." + extension.split("+").shift();
}

/**
 * Triggers browser "Download file" action
 * using a content of a file, provided by
 * "archive" parameter
 * @param {} archive - BLOB of file to download
 */
// function downloadArchive(archive) {
//     console.log("downloadArchive()");
//     const link = window.document.createElement('a');
//     link.href = window.URL.createObjectURL(archive);
//     link.download = "images.zip";
//     //    link.download = "MyText.txt";
//     document.body.appendChild(link);
//     link.click();
//     window.URL.revokeObjectURL(link.href);
//     document.body.removeChild(link);
// }

// var blob = new Blob(["My text"], { type: 'text/plain'});

async function button() {
    [fileHandle] = await window.showOpenFilePicker();
    console.log(fileHandle.kind);
}

document.getElementById("saveAs")
    .addEventListener("click", async () => {
        try {
            // const handle = await window.showSaveFilePicker();
            // console.log("handle: ");
            // console.log(handle);

            saveAs();

        } catch (err) {
            alert(err.message)
        }
    })

async function save(blob, fileHandle) {
    console.log("save()");
    let stream = await fileHandle.createWritable();

    console.log("stream: ");
    console.log(stream);

    await stream.write(blob);
    await stream.close();
}

// my comment

async function saveAs() {
    console.log("saveAs()");

    const urls = getSelectedUrls();

    console.log("urls: ");
    console.log(urls);
    
    for (let index in urls) {
        console.log("index: ");
        console.log(index);
        try {
            const url = urls[index];
            const response = await fetch(url);
            const blob = await response.blob();
            const responseURL = response.url;
            const responseURLasString = response.url.toString()
            let URLfileName = responseURL.substring(responseURL.lastIndexOf('/') + 1);

            // Remove illegal filename characters
            URLfileName = URLfileName.replace(/([^a-z0-9.]+)/gi, '_');

            const URLfileNameText = URLfileName + "_URL.txt";
            const URLtextFileBlob = new Blob([responseURLasString], { type: 'text/plain' });

            console.log("blob: ");
            console.log(blob);
            console.log("fileHandle: ");
            console.log(fileHandle);

            const directoryHandle = await window.showDirectoryPicker();
            console.log("directoryHandle: ", directoryHandle);

            const imageFileHandle = await directoryHandle.getFileHandle(URLfileName, { create: true });

            console.log("URLfileName: ", URLfileName);
            console.log("imageFileHandle: ", imageFileHandle);
            save(blob, imageFileHandle);
 
            // const URL_textFileHandle = await directoryHandle.getFileHandle('MyPictureA_URL.txt', { create: true });
            const URL_textFileHandle = await directoryHandle.getFileHandle(URLfileNameText, { create: true });

            console.log("URL_textFileHandle: ", URL_textFileHandle);
            console.log("URLfileNameText: ", URLfileNameText);
            save(URLtextFileBlob, URL_textFileHandle);

        } catch (err) {
            console.error(err);
        }
    }
}
