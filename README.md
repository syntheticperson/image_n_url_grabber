# Image 'n URL Grabber
Google Chrome extension used to grab a list of images from current webpage, then:  

- Allows you to specify or create a local directory to download the image to on your computer.
- It also saves a text file with the source URL for each image.

Forked from this GitHub Repository.

https://github.com/AndreyGermanov/image_grabber

**Instructions**

Go to a web page where you want to download the images.

![UI](assets/Wikipedia_cat.png)

Left mouse click the Image n' URL Grabber extension button.

![UI](assets/Image_n_URL_Grabber_popup.png)

Left mouse click the Grab Now button.  
This brings up a page with thumbnails of all the images found on the source page.  

![UI](assets/Image_n_URL_Grabber_ThumbNails.png)

Select the images you want to download with their URLs.

![UI](assets/ImagesSelected.png)

Left mouse click the Save As button.  
This brings up a Finder window.

![UI](assets/SelectFolderToView.png)

Navigate to the folder where you want to save the images and URL text files.  
Note: you can also use this window to create a new directory.
  
Left mouse  the Select button.  
  
This brings up a Dialog that ask you if it's ok for this extension to view files in the directory.  
Select View Files.  
    
![UI](assets/LetSiteViewFiles.png)

This brings up another dialog asking for permission to save changes to files.  
Select Save Changes.  
  
![UI](assets/SaveChanges.png)  
  
When you open the directory on your computer, you should now see the image and URL text files.  
  
![UI](assets/CatsDirWithImagesAndURLtextFiles.png)  
  
